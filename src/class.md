# Class 4.12 -> 11.12 Todolist project

Premise: Create a TODO list application, that contains the following:
- Input for writing the text of the task
- A button for creating a task, with text provided for the input
- The input clears out after creating a new task
- A list of created tasks
- A checkbox for completing tasks
- The checkbox can be clicked repeatedly to uncomplete the task
- BONUS: A list of completed task that are striked through
- BONUS: The tasks stay after the user refreshes the page

Task:
[] Task name 
[x] Task name done