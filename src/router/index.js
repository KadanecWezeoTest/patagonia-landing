import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import OutdoorView from '../views/OutdoorView.vue'
import TodoView from '../views/TodoView.vue'
import BootstrapView from '../views/BootstrapView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/outdoor',
      name: 'outdoor',
      component: OutdoorView
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    },
    {
      path: '/todo',
      name: 'todo',
      component: TodoView

    },
    {
      path: '/bootstrap',
      name: 'bootstrap',
      component: BootstrapView
    }
  ]
})

export default router
